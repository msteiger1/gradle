## [2.6.3](https://gitlab.com/to-be-continuous/gradle/compare/2.6.2...2.6.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([72e1011](https://gitlab.com/to-be-continuous/gradle/commit/72e1011c6c4accddf10561d94baebd8a1431aa29))

## [2.6.2](https://gitlab.com/to-be-continuous/gradle/compare/2.6.1...2.6.2) (2024-2-15)


### Bug Fixes

* publish should be enabled by default ([86c6777](https://gitlab.com/to-be-continuous/gradle/commit/86c67772ead4f54fc00843e6dc9393bed353cc94))

## [2.6.1](https://gitlab.com/to-be-continuous/gradle/compare/2.6.0...2.6.1) (2024-1-30)


### Bug Fixes

* sanitize variable substitution pattern ([a37e6f2](https://gitlab.com/to-be-continuous/gradle/commit/a37e6f2056491652c449bd57d01320a8859057d7))

# [2.6.0](https://gitlab.com/to-be-continuous/gradle/compare/2.5.0...2.6.0) (2024-1-27)


### Features

* migrate to CI/CD component ([d8bc8bd](https://gitlab.com/to-be-continuous/gradle/commit/d8bc8bd92838b3a18ab7a4aecc17f652ab4e1cb9))

# [2.5.0](https://gitlab.com/to-be-continuous/gradle/compare/2.4.2...2.5.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([c9a0c02](https://gitlab.com/to-be-continuous/gradle/commit/c9a0c0244e4b70e93e2982d0df648f57f1ed5223))

## [2.4.2](https://gitlab.com/to-be-continuous/gradle/compare/2.4.1...2.4.2) (2023-10-18)


### Bug Fixes

* add repository for cyclonedx-gradle-plugin ([bdbee0f](https://gitlab.com/to-be-continuous/gradle/commit/bdbee0f6e7cf8a711120f3c5d86bab70677f7dae))

## [2.4.1](https://gitlab.com/to-be-continuous/gradle/compare/2.4.0...2.4.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([7289f7c](https://gitlab.com/to-be-continuous/gradle/commit/7289f7c8466fea771ef9728374ef6997bc99655d))

# [2.4.0](https://gitlab.com/to-be-continuous/gradle/compare/2.3.0...2.4.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([78aaadc](https://gitlab.com/to-be-continuous/gradle/commit/78aaadc3377977e040ded7255f84ea5fd237ad61))

# [2.3.0](https://gitlab.com/to-be-continuous/gradle/compare/2.2.2...2.3.0) (2023-05-11)


### Features

* add SonarQube job ([d2c9df5](https://gitlab.com/to-be-continuous/gradle/commit/d2c9df5ec0904648ef637f7558bdb5bc3899a4cd))

## [2.2.2](https://gitlab.com/to-be-continuous/gradle/compare/2.2.1...2.2.2) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([7662bd6](https://gitlab.com/to-be-continuous/gradle/commit/7662bd6f67467be207cfe20c7eaf27dea3f9a8d1))

## [2.2.1](https://gitlab.com/to-be-continuous/gradle/compare/2.2.0...2.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([0e1eb95](https://gitlab.com/to-be-continuous/gradle/commit/0e1eb95b2aa5d3713d4e19b549f6949b365e8eea))

# [2.2.0](https://gitlab.com/to-be-continuous/gradle/compare/2.1.0...2.2.0) (2022-12-14)


### Features

* add a job generating software bill of materials ([7812897](https://gitlab.com/to-be-continuous/gradle/commit/78128975e65652c0a8d54011c0be2de2a547ac28))

# [2.1.0](https://gitlab.com/to-be-continuous/gradle/compare/2.0.0...2.1.0) (2022-11-26)


### Features

* **dep-check:** support disabling dependency-check ([2f80ba3](https://gitlab.com/to-be-continuous/gradle/commit/2f80ba3e683d68674dcb52abf0da864ef936735c)), closes [#11](https://gitlab.com/to-be-continuous/gradle/issues/11)

# [2.0.0](https://gitlab.com/to-be-continuous/gradle/compare/1.4.0...2.0.0) (2022-08-05)


### Features

* adaptive pipeline ([5ffb495](https://gitlab.com/to-be-continuous/gradle/commit/5ffb495c951fe461f03e1e0bb324dc305cc1361a))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.4.0](https://gitlab.com/to-be-continuous/gradle/compare/1.3.1...1.4.0) (2022-05-01)


### Features

* configurable tracking image ([a9131b4](https://gitlab.com/to-be-continuous/gradle/commit/a9131b454cc2fab2344d9719e52193fe1d2cd574))

## [1.3.1](https://gitlab.com/to-be-continuous/gradle/compare/1.3.0...1.3.1) (2022-02-25)


### Bug Fixes

* keep build artifacts regardless of build job status ([fe12c8c](https://gitlab.com/to-be-continuous/gradle/commit/fe12c8cf0881b5b6c130666c9f50ff934c6e7c7d))

# [1.3.0](https://gitlab.com/to-be-continuous/gradle/compare/1.2.1...1.3.0) (2021-12-10)


### Features

* add project root directory variable ([fa0c30a](https://gitlab.com/to-be-continuous/gradle/commit/fa0c30af30a67ad64e28c8b9bb24a2f967d5f20b))

## [1.2.1](https://gitlab.com/to-be-continuous/gradle/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([dfee6cb](https://gitlab.com/to-be-continuous/gradle/commit/dfee6cb401358e46316a1d7e018cfa6bb03897c7))

## [1.2.0](https://gitlab.com/to-be-continuous/gradle/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([ccab558](https://gitlab.com/to-be-continuous/gradle/commit/ccab5588dff93aaed5528a2c10c5aa9a6ddaba47))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gradle/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([76b028c](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/76b028cd71852e2e42ef945f5eb556a02214a65d))

## 1.0.0 (2021-05-06)

### Features

* initial release ([4c573b7](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/4c573b7b8a8017c449cf6ae70e92830d989ceced))
